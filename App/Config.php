<?php

namespace App;

/**
 * Application configuration
 *
 * PHP version 8.0.2
 */
class Config
{
    /**
     * Database host
     * @var string
     */
    const DB_HOST = 'localhost';

    /**
     * Database name
     * @var string
     */
    const DB_NAME = 'webstore';

    /**
     * Database user
     * @var string
     */
    const DB_USER = 'webstore_user';

    /**
     * Database password
     * @var string
     */
    const DB_PASSWORD = 'webstore_user_password';
}

?>
