<?php

namespace App;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

/**
 * Logger class.
 * 
 * PHP version 8.0.2
 */
class AppLogger
{
    /**
     * This function add a info log message into the log file.
     * Info log messages are interesting events. Examples: User logs in, SQL logs.
     *
     * @param string $logMessage The message that will be added.
     * @param array $dataArray An array with data that you desire to be shown in the log message.
     * @return void
     */
    public static function addDatabaseInfoLog($logMessage, $dataArray = [])
    {
        // Create the logger
        $logger = new Logger('app_logger');
        // Now add some handlers
        $logger->pushHandler(new StreamHandler(__DIR__.'/LogFiles/database.log', Logger::INFO));
        $logger->info($logMessage, $dataArray);
    }

    /**
     * This function add a warning log message into the log file.
     * Warning log messages are exceptional occurrences that are not errors. Examples: Use of deprecated APIs, poor
     * use of an API, undesirable things that are not necessarily wrong.
     *
     * @param string $logMessage The message that will be added.
     * @param array $dataArray An array with data that you desire to be shown in the log message.
     * @return void
     */
    public static function addDatabaseWarningLog($logMessage, $dataArray = [])
    {
        // Create the logger
        $logger = new Logger('app_logger');
        // Now add some handlers
        $logger->pushHandler(new StreamHandler(__DIR__.'/LogFiles/database.log', Logger::WARNING));
        $logger->warning($logMessage, $dataArray);
    }

    /**
     * This function add a info log message into the log file.
     * Info log messages are interesting events. Examples: User logs in, SQL logs.
     *
     * @param string $logMessage The message that will be added.
     * @param array $dataArray An array with data that you desire to be shown in the log message.
     * @return void
     */
    public static function addApplicationInfoLog($logMessage, $dataArray = [])
    {
        // Create the logger
        $logger = new Logger('app_logger');
        // Now add some handlers
        $logger->pushHandler(new StreamHandler(__DIR__.'/LogFiles/application.log', Logger::INFO));
        $logger->info($logMessage, $dataArray);
    }

    /**
     * This function add a warning log message into the log file.
     * Warning log messages are exceptional occurrences that are not errors. Examples: Use of deprecated APIs, poor
     * use of an API, undesirable things that are not necessarily wrong.
     *
     * @param string $logMessage The message that will be added.
     * @param array $dataArray An array with data that you desire to be shown in the log message.
     * @return void
     */
    public static function addApplicationWarningLog($logMessage, $dataArray = [])
    {
        // Create the logger
        $logger = new Logger('app_logger');
        // Now add some handlers
        $logger->pushHandler(new StreamHandler(__DIR__.'/LogFiles/application.log', Logger::WARNING));
        $logger->warning($logMessage, $dataArray);
    }
}

?>