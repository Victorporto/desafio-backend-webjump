<?php

namespace App\Model\Service;

use App\AppLogger;
use App\Model\Object\ProductObject;
use App\Model\Database\ProductDatabase;
use App\Model\Database\CategoryDatabase;
use App\Model\Database\ProductCategoryDatabase;

/**
 * ProductService class.
 * It handles business logic about the products on the application.
 *
 * PHP version 8.0.2
 */
class ProductService
{
    /**
     * This attribute contain the list of errors from last executed function.
     *
     * @var array
     */
    private $errorList = [];
    
    /**
     * Get the $errors attribute.
     *
     * @return array List of errors.
     */
    public function getErrorList()
    {
        return $this->errorList;
    }

    /**
     * Set the $errorList attribute.
     *
     * @param array $errorList A list of errors to set the $errorList attribute.
     * @return void
     */
    private function setErrors($errorList)
    {
        $this->errorList = $errorList;
    }

    /**
     * This function add a string into $errorList attribute.
     *
     * @param string $error The error string to add.
     * @return void
     */
    private function addToErrorList($error)
    {
        $this->errorList[] = $error;
    }

    /**
     * This method add a product object. If false is returned, then use $this->getErrorList() to see the errors.
     *
     * @param string $sku The sku of the product.
     * @param string $name The name of the product.
     * @param string $price The price of the product.
     * @param string $description The description of the product.
     * @param string $quantity The quantity of products.
     * @param array $categoryCodeList A list of category code.
     * @return boolean True if added, false otherwise.
     */
    public function add($sku, $name, $price, $description, $quantity, $categoryCodeList)
    {
        $product = $this->formatUserInput($sku, $name, $price, $description, $quantity, $categoryCodeList);
        return $this->validateInsertion($product) ? ProductDatabase::insert($product) : false;
    }

    /**
     * This function format the data input done by the user while trying to add/edit a ProductObject.
     *
     * @param string $sku The code inputed by the user
     * @param string $name The name inputed by the user
     * @param string $price The name inputed by the user
     * @param string $description The name inputed by the user
     * @param string $quantity The name inputed by the user
     * @param array $categoryCodeList The name inputed by the user
     * @return ProductObject
     */
    protected function formatUserInput($sku, $name, $price, $description, $quantity, $categoryCodeList)
    {
        // Removing blank spaces from beginning and end
        $sku = trim($sku);
        $name = trim($name);
        $description = trim($description);

        if (empty($sku)) { $sku = null; }
        if (empty($name)) { $name = null; }
        if (empty($price)) { $price = 0; }
        if (empty($description)) { $description = null; }
        if (empty($quantity)) { $quantity = 0; }

        $categoryList = [];
        foreach ($categoryCodeList as $code) {
            $categoryList[] = CategoryDatabase::selectByCode($code);
        }
        
        return new ProductObject($sku, $name, $price, $description, $quantity, $categoryList);
    }

    /**
     * This method validate a instance of ProductObject to check if it can be inserted into database.
     *
     * @param ProductObject $product The product object.
     * @return array The array of errors. Empty if no validation errors occurred.
     */
    protected function validateInsertion($product)
    {
        $this->setErrors([]);
        
        // Validation fail if sku is empty
        if (empty($product->getSku())) {
            $this->addToErrorList('The Product must have a sku code.');
        }

        // Validation fail if sku is longer than 30 characters
        if ( ! empty($product->getSku()) && strlen($product->getSku()) > 30) {
            $this->addToErrorList("The Product sku code can't be longer than 30 characters.");
        }

        // Validation fail if name is empty
        if (empty($product->getName())) {
            $this->addToErrorList('The Product must have a name.');
        }

        // Validation fail if price is bigger than 9999.99
        if ( ! empty($product->getPrice()) && $product->getPrice() > 9999.99) {
            $this->addToErrorList("The Product price can't be bigger than R$ 9999.99.");
        }

        // Validation fail if price is smaller than 0.00
        if ( ! empty($product->getPrice()) && $product->getPrice() < 0) {
            $this->addToErrorList("The Product price can't be smaller than R$ 0.00.");
        }

        // Validation fail if quantity is smaller than 0.00
        if ( ! empty($product->getQuantity()) && $product->getQuantity() < 0) {
            $this->addToErrorList("The Product quantity can't be smaller than 0.");
        }

        // Validation fail if database already has a product with this sku
        if (! empty($product->getSku()) && ProductDatabase::selectBySku($product->getSku())) {
            $this->addToErrorList("The SKU '" . $product->getSku() . "' is already in use for another Product.");
        }

        // Validation fail if the product don't belong to any category
        if (empty($product->getCategoryList())) {
            $this->addToErrorList("The Product must have at least one category.");
        }

        // Validation fail if any object in $this->categoryList isn't present in database anymore
        foreach ($product->getCategoryList() as $category) {

            if (CategoryDatabase::selectByCode($category->getCode()) === false) {
                $this->addToErrorList("One of the selected categories doesn't exist in the database.");
            }
        }

        if (empty($this->getErrorList())) {
            return true;
        } else {
            // The log message
            $logMessage = "A `App\Model\Object\ProductObject` object failed data validation in " .
            "`App\Model\Service\ProductService::validateInsertion()`.";
            // Data array for a better context of what happened
            $dataArray = ['CategoryObject' => $product->toArray(), 'FailedValidations' => $this->getErrorList()];

            AppLogger::addApplicationInfoLog($logMessage, $dataArray);
            return false;
        }
    }

    /**
     * This method edit a product object. If false is returned, then use $this->getErrorList() to see the errors.
     *
     * @param string $sku The sku of the product.
     * @param string $name The name of the product.
     * @param string $price The price of the product.
     * @param string $description The description of the product.
     * @param string $quantity The quantity of products.
     * @param array $categoryCodeList A list of category code.
     * @return boolean True if edited, false otherwise.
     */
    public function edit($sku, $name, $price, $description, $quantity, $categoryCodeList)
    {
        $product = $this->formatUserInput($sku, $name, $price, $description, $quantity, $categoryCodeList);
        
        if ($this->validateEdition($product)) {
            if (ProductDatabase::update($product)) {
                $selectedCategoryList = $product->getCategoryList(); // The categories selected by the user
                $currentCategoryCodeList = []; // The current list on database

                // Get only the code of all productCategory found on database where $product->sku was found
                $productCategoryList = ProductCategoryDatabase::selectAllByProductSku($product->getSku());
                foreach ($productCategoryList as $productCategory) {
                    $currentCategoryCodeList[] = $productCategory->getCategoryCode();
                }

                $categoryCodeListNotChanged = []; // Category codes that appear on database and in the selected categories of the user
                
                // Fill $categoryCodeListNotChanged
                foreach ($selectedCategoryList as $category) {
                    in_array($category->getCode(), $currentCategoryCodeList) ? $categoryCodeListNotChanged[] = $category->getCode() : null ;
                }

                // Deleting from product_category, rows in the database that no longer have relation with $product
                foreach ($currentCategoryCodeList as $categoryCode) {
                    
                    if ( ! in_array($categoryCode, $categoryCodeListNotChanged)) {
                        ProductCategoryDatabase::deleteBySkuAndCode($product->getSku(), $categoryCode);
                    }
                }

                // Add rows to product_category of new relations of categories with $product
                foreach ($selectedCategoryList as $category) {
                    
                    if ( ! in_array($category->getCode(), $currentCategoryCodeList)) {
                        ProductCategoryDatabase::add($product->getSku(), $category->getCode());
                    }
                }

                return true;
            }
        }

        return false;
    }

    /**
     * This method edit a category object. If false is returned, then use $this->getErrorList() to see the errors.
     *
     * @param ProductObject $product The product been edited.
     * @return void
     */
    protected function validateEdition($product)
    {
        $this->setErrors([]);

        // Validation fail if sku is empty
        if (empty($product->getSku())) {
            $this->addToErrorList("It's not possible to edit without the product sku.");
        }

        // Validation fail if sku is longer than 30 characters
        if ( ! empty($product->getSku()) && strlen($product->getSku()) > 30) {
            $this->addToErrorList("The Product sku code can't be longer than 30 characters.");
        }

        // Validation fail if name is empty
        if (empty($product->getName())) {
            $this->addToErrorList('The product must have a name.');
        }

        // Validation fail if price is bigger than 9999.99
        if ( ! empty($product->getPrice()) && $product->getPrice() > 9999.99) {
            $this->addToErrorList("The Product price can't be bigger than R$ 9999.99.");
        }

        // Validation fail if price is smaller than 0.00
        if ( ! empty($product->getPrice()) && $product->getPrice() < 0) {
            $this->addToErrorList("The Product price can't be smaller than R$ 0.00.");
        }

        // Validation fail if quantity is smaller than 0.00
        if ( ! empty($product->getQuantity()) && $product->getQuantity() < 0) {
            $this->addToErrorList("The Product quantity can't be smaller than 0.");
        }

        // Validation fail if the product don't belong to any category
        if (empty($product->getCategoryList())) {
            $this->addToErrorList("The Product must have at least one category.");
        }

        // Validation fail if any object in $this->categoryList isn't present in database anymore
        foreach ($product->getCategoryList() as $category) {

            if (CategoryDatabase::selectByCode($category->getCode()) === false) {
                $this->addToErrorList("One of the selected categories doesn't exist in the database.");
            }
        }

        // Validation fail if there isn't a product with that sku in database.
        if ( ! empty($product->getSku()) && empty(ProductDatabase::selectBySku($product->getSku())))
        {
            $this->addToErrorList("It's not possible to edit the sku of the product.");
        }

        if (empty($this->getErrorList())) {
            return true;
        } else {
            // The log message
            $logMessage = "An attempt to edit a ProductObject from the database has fail validation in " .
            "`App\Model\Service\ProductService::validateEdition()`.";
            
            // Data array for a better context of what happened
            $dataArray = [
                'editedProductData' => $product->toArray(),
                'FailedValidations' => $this->getErrorList()
            ];

            AppLogger::addApplicationInfoLog($logMessage, $dataArray);
            return false;
        }
    }

    /**
     * This method delete a product object. If false is returned, then use $this->getErrorList() to see the errors.
     *
     * @param string $sku The sku of the product
     * @return void
     */
    public function delete($sku)
    {
        $sku = $this->formatDeleteUserInput($sku);
        return $this->validateDeletion($sku) ? ProductDatabase::deleteBySku($sku) : false;
    }

    /**
     * This function format the data input done by the user while trying to delete a ProductObject.
     *
     * @param string $sku The code inputed by the user
     * @return string
     */
    protected function formatDeleteUserInput($sku)
    {
        // Removing blank spaces from beginning and end
        $sku = trim($sku);
        if (empty($sku)) { $sku = null; }
        return $sku;
    }

    /**
     * This method validate the $sku to see if a ProductObject exist, so it can be deleted from database.
     *
     * @param string $sku The sku of the product object.
     * @return array The array of errors. Empty if no validation errors occurred.
     */
    protected function validateDeletion($sku)
    {
        $this->setErrors([]);

        // Validation fail if database don't have a product with this sku
        if (ProductDatabase::selectBySku($sku) == false) {
            $this->addToErrorList("There is no product with sku as '" . $sku . "' to be deleted.");
        }

        if (empty($this->getErrorList())) {
            return true;
        } else {
            // The log message
            $logMessage = "An attempt to delete a ProductObject from the database has fail validation in " .
            "`App\Model\Service\ProductService::validateDeletion()`.";
            // Data array for a better context of what happened
            $dataArray = ['Sku' => $sku, 'FailedValidations' => $this->getErrorList()];

            AppLogger::addApplicationWarningLog($logMessage, $dataArray);
            return false;
        }
    }

}

?>