<?php

namespace App\Model\Service;

use App\AppLogger;
use App\Model\Object\CategoryObject;
use App\Model\Database\CategoryDatabase;
use App\Model\Database\ProductCategoryDatabase;

/**
 * CategoryService class.
 * It handles business logic about the categories on the application.
 *
 * PHP version 8.0.2
 */
class CategoryService
{
    /**
     * This attribute contain the list of errors from last executed function.
     *
     * @var array
     */
    private $errorList = [];
    
    /**
     * Get the $errors attribute.
     *
     * @return array List of errors.
     */
    public function getErrorList()
    {
        return $this->errorList;
    }

    /**
     * Set the $errorList attribute.
     *
     * @param array $errorList A list of errors to set the $errorList attribute.
     * @return void
     */
    private function setErrors($errorList)
    {
        $this->errorList = $errorList;
    }

    /**
     * This function add a string into $errorList attribute.
     *
     * @param string $error The error string to add.
     * @return void
     */
    private function addToErrorList($error)
    {
        $this->errorList[] = $error;
    }

    /**
     * This method add a category object. If false is returned, then use $this->getErrorList() to see the errors.
     *
     * @param string $code The category code.
     * @param string $name The category name.
     * @return boolean True if added, false otherwise.
     */
    public function add($code, $name)
    {
        $category = $this->formatUserInput($code,$name);
        return $this->validateInsertion($category) ? CategoryDatabase::insert($category) :  false ;
    }

    /**
     * This function format the data input done by the user while trying to add/edit a CategoryObject.
     *
     * @param string $code The code inputed by the user
     * @param string $name The name inputed by the user
     * @return CategoryObject
     */
    protected function formatUserInput($code, $name)
    {
        // Removing blank spaces from beginning and end
        $code = trim($code);
        $name = trim($name);

        if (empty($code)) { $code = null; }
        if (empty($name)) { $name = null; }
        
        return new CategoryObject($code, $name);
    }

    /**
     * This method validate a instance of CategoryObject to check if it can be inserted into database.
     *
     * @param CategoryObject $category The category object.
     * @return array The array of errors. Empty if no validation errors occurred.
     */
    protected function validateInsertion($category)
    {
        $this->setErrors([]);

        // Validation fail if code is empty
        if (empty($category->getCode())) {
            $this->addToErrorList('The Category must have a code.');
        }

        // Validation fail if code is longer than 100 characters
        if ( ! empty($category->getCode()) && strlen($category->getCode()) > 100) {
            $this->addToErrorList("The Category code can't be longer than 100 characters.");
        }

        // Validation fail if name is empty
        if (empty($category->getName())) {
            $this->addToErrorList('The Category must have a name.');
        }

        // Validation fail if database already has a category with this code
        if (! empty($category->getCode()) && CategoryDatabase::selectByCode($category->getCode())) {
            $this->addToErrorList("The code '" . $category->getCode() . "' is already in use for another Category.");
        }

        if (empty($this->getErrorList())) {
            return true;
        } else {
            // The log message
            $logMessage = "A `App\Model\Object\CategoryObject` object failed data validation in " .
            "`App\Model\Service\CategoryService::validateInsertion()`.";
            // Data array for a better context of what happened
            $dataArray = ['CategoryObject' => $category->toArray(), 'FailedValidations' => $this->getErrorList()];

            AppLogger::addApplicationInfoLog($logMessage, $dataArray);
            return false;
        }
    }

    /**
     * This method edit a category object. If false is returned, then use $this->getErrorList() to see the errors.
     *
     * @param string $code The category code.
     * @param string $name The category name.
     * @return boolean True if edited, false otherwise.
     */
    public function edit($code, $name)
    {
        $category = $this->formatUserInput($code, $name);
        return $this->validateEdition($category) ? CategoryDatabase::update($category) :  false ;
    }

    /**
     * This method edit a category object. If false is returned, then use $this->getErrorList() to see the errors.
     *
     * @param CategoryObject $category The category been edited.
     * @return void
     */
    protected function validateEdition($category)
    {
        $this->setErrors([]);

        // Validation fail if code is empty
        if (empty($category->getCode())) {
            $this->addToErrorList("It's not possible to edit without the category code.");
        }

        // Validation fail if name is empty
        if (empty($category->getName())) {
            $this->addToErrorList('The Category must have a name.');
        }

        // Validation fail if there isn't a category with that code in database.
        if ( ! empty($category->getCode()) && empty(CategoryDatabase::selectByCode($category->getCode())))
        {
            $this->addToErrorList("It's not possible to edit the category code.");
        }

        if (empty($this->getErrorList())) {
            return true;
        } else {
            // The log message
            $logMessage = "An attempt to edit a CategoryObject from the database has fail validation in " .
            "`App\Model\Service\CategoryService::validateEdition()`.";
            
            // Data array for a better context of what happened
            $dataArray = [
                'editedCategoryData' => $category->toArray(),
                'FailedValidations' => $this->getErrorList()
            ];

            AppLogger::addApplicationInfoLog($logMessage, $dataArray);
            return false;
        }
    }

    /**
     * This method delete a category object. If false is returned, then use $this->getErrorList() to see the errors.
     *
     * @param string $code The code of the category
     * @return void
     */
    public function delete($code)
    {
        $code = $this->formatDeleteUserInput($code);
        return $this->validateDeletion($code) ? CategoryDatabase::deleteByCode($code) : false;
    }

    /**
     * This function format the data input done by the user while trying to delete a CategoryObject.
     *
     * @param string $code The code inputed by the user
     * @return string
     */
    protected function formatDeleteUserInput($code)
    {
        // Removing blank spaces from beginning and end
        $code = trim($code);
        if (empty($code)) { $code = null; }
        return $code;
    }

    /**
     * This method validate the $code to see if a CategoryObject exist, so it can be deleted from database.
     *
     * @param string $code The code of the category object.
     * @return array The array of errors. Empty if no validation errors occurred.
     */
    protected function validateDeletion($code)
    {
        $this->setErrors([]);
        
        // Validation fail if database don't have a category with this code
        if (CategoryDatabase::selectByCode($code) == false) {
            $this->addToErrorList("There is no category with code as '" . $code . "' to be deleted.");
        }

        // Validation fail if there is still relationship between the category the user is trying to delete and products
        // in the database
        if ( ! empty(ProductCategoryDatabase::selectAllByCategoryCode($code))) {
            $this->addToErrorList("You cannot delete the category of code as '" . $code . "' because there is products " .
            "that still belong to it.");
        }

        if (empty($this->getErrorList())) {
            return true;
        } else {
            // The log message
            $logMessage = "An attempt to delete a CategoryObject from the database has fail validation in " .
            "`App\Model\Service\CategoryService::validateDeletion()`.";
            // Data array for a better context of what happened
            $dataArray = ['code' => $code, 'FailedValidations' => $this->getErrorList()];

            AppLogger::addApplicationInfoLog($logMessage, $dataArray);
            return false;
        }
    }
}

?>