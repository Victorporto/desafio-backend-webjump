<?php

namespace App\Model\Util;

/**
 * Utility class that sanitize data coming from the user.
 *
 * PHP version 8.0.2
 */
class SanitizeUtil
{

    /**
     * Sanitize the data using 'htmlspecialchars' function in a recursive manner (works on arrays).
     *
     * @param mixed $value The data to be sanitized.
     *
     * @return mixed $value The result properly sanitized.
     */
    public static function recursiveHtmlSpecialChars($value)
    {
        if (is_array($value)) {
            return array_map('static::recursiveHtmlSpecialChars', $value);
        } else {
            return htmlspecialchars($value);
        }
    }
    
}

?>
