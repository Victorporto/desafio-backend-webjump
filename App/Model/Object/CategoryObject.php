<?php

namespace App\Model\Object;


/**
 * CategoryObject class.
 * 
 * This class define the basic properties of category objects. 
 *
 * PHP version 8.0.2
 */
class CategoryObject
{
    private $code;
    private $name;

    /**
     * Contructor method
     *
     * @param string $code The code of the category.
     * @param string $name The name of the category.
     */
    public function __construct($code = null, $name = null)
    {
        $this->code = $code;
        $this->name = $name;
    }

    /**
     * Get the value of code
     *
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set the value of code
     *
     * @param mixed $code 
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * Get the value of name
     *
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @param mixed $name 
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Convert the object into an array.
     *
     * @return array
     */
    public function toArray()
    {
        return ['code' => $this->getCode(), 'name' => $this->getName()];
    }
}

?>