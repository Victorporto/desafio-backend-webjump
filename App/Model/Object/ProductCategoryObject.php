<?php

namespace App\Model\Object;

class ProductCategoryObject
{
    private $productSku;
    private $categoryCode;

    /**
     * Contructor method
     *
     * @param string $productSku The sku of the product.
     * @param string $categoryCode The code of the category.
     */
    public function __construct($productSku = null, $categoryCode = null)
    {
        $this->productSku = $productSku;
        $this->categoryCode = $categoryCode;
    }

    /**
     * Get the value of productSku
     *
     * @return mixed
     */
    public function getProductSku()
    {
        return $this->productSku;
    }

    /**
     * Set the value of productSku
     *
     * @param mixed $productSku 
     */
    public function setProductSku($productSku)
    {
        $this->productSku = $productSku;
    }

    /**
     * Get the value of categoryCode
     *
     * @return mixed
     */
    public function getCategoryCode()
    {
        return $this->categoryCode;
    }

    /**
     * Set the value of categoryCode
     *
     * @param mixed $categoryCode 
     */
    public function setCategoryCode($categoryCode)
    {
        $this->categoryCode = $categoryCode;
    }
}

?>