<?php

namespace App\Model\Object;

use App\Model\Object\CategoryObject;

/**
 * ProductObject class.
 * 
 * This class define the basic properties of product objects. 
 *
 * PHP version 8.0.2
 */
class ProductObject
{
    private $sku;
    private $name;
    private $price;
    private $description;
    private $quantity;
    private $categoryList = [];

    /**
     * Contructor method
     *
     * @param string $sku The sku of the product.
     * @param string $name The name of the product.
     * @param string $price The price of the product.
     * @param string $description The description of the product.
     * @param int $quantity The quantity of the product.
     * @param array $categoryList The CategoryObject list of the product.
     */
    public function __construct(
        $sku = null,
        $name = null,
        $price = null,
        $description = null,
        $quantity = null,
        $categoryList = [])
    {
        $this->sku = $sku;
        $this->name = $name;
        $this->price = $price;
        $this->description = $description;
        $this->quantity = $quantity;
        $this->categoryList = $categoryList;
    }

    /**
     * Get the value of sku
     *
     * @return mixed
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * Set the value of sku
     *
     * @param mixed $sku 
     */
    public function setSku($sku)
    {
        $this->sku = $sku;
    }

    /**
     * Get the value of name
     *
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @param mixed $name 
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get the value of price
     *
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set the value of price
     *
     * @param mixed $price 
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * Get the value of description
     *
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set the value of description
     *
     * @param mixed $description 
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Get the value of quantity
     *
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set the value of quantity
     *
     * @param mixed $quantity 
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * Get the value of categoryList
     *
     * @return mixed
     */
    public function getCategoryList()
    {
        return $this->categoryList;
    }

    /**
     * Set the value of categoryList
     *
     * @param mixed $categoryList 
     */
    public function setCategoryList($categoryList)
    {
        $this->categoryList = $categoryList;
    }

    /**
     * Add a instance of CategoryObject into categoryList
     *
     * @param CategoryObject $category
     */
    public function addToCategoryList($category)
    {
        $this->categoryList[] = $category;
    }

    /**
     * Convert the object into an array.
     *
     * @return array
     */
    public function toArray()
    {
        $objectArray = [
            'sku' => $this->getSku(),
            'name' => $this->getName(),
            'price' => $this->getPrice(),
            'description' => $this->getDescription(),
            'quantity' => $this->getQuantity()
        ];

        // Converting every CategoryObject in $this->categoryList into an array.
        $categoryList = [];
        foreach ($this->getCategoryList() as $category) {
            $categoryList[] = $category->toArray();
        }
        $objectArray['categoryList'] = $categoryList;

        return $objectArray;
    }
}

?>