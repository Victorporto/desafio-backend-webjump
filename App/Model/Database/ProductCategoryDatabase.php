<?php

namespace App\Model\Database;

use PDO;
use PDOException;
use App\AppLogger;
use App\Model\Object\ProductObject;
use App\Model\Object\ProductCategoryObject;

/**
 * ProductCategoryDatabase class.
 * 
 * It can access the database to Read data of product_category in the database.
 *
 * PHP version 8.0.2
 */
class ProductCategoryDatabase extends \Core\Database
{
    /**
     * This method insert a row in the product_category table from the database.
     *
     * @param string $productSku The sku of the product.
     * @param string $categoryCode The code of the category.
     * @return boolean
     */
    public static function add($productSku, $categoryCode){
        try {
            $sql = "INSERT INTO product_category (product_sku, category_code)
                    VALUES (:product_sku, :category_code);";
            $conn = static::getConn();
            $stmt = $conn->prepare($sql);
            $stmt->bindParam(':product_sku', $productSku, PDO::PARAM_STR);
            $stmt->bindParam(':category_code', $categoryCode, PDO::PARAM_STR);
            $stmt->execute();

            // The log message
            $logMessage = "A row was successfully inserted into `product_category` database table in " .
            "`App\Model\Database\ProductCategoryDatabase::add()`.";
            // Data array for a better context of what happened
            $dataArray = ['product_sku' => $productSku, 'category_code' => $categoryCode];
            AppLogger::addDatabaseInfoLog($logMessage, $dataArray);
            
            return true;
        } catch (PDOException $e) {
            // The log message
            $logMessage = "A PDOException occurred while trying to insert a row into `product_category` database table " .
            "in `App\Model\Database\ProductCategoryDatabase::add()`.";
            // Data array for a better context of what happened
            $dataArray = [
                'product_sku' => $productSku,
                'category_code' => $categoryCode,
                'PDOExceptionMessage' => $e->getMessage()
            ];
            AppLogger::addDatabaseWarningLog($logMessage, $dataArray);
        }
        return false;
    }

    /**
     * This method uses the product object sku and the categoryList in it to insert the relationship into the database.
     *
     * @param ProductObject $product The product object.
     * @return boolean
     */
    public static function insertProductCategoryListOfProduct($product)
    {
        foreach ($product->getCategoryList() as $category) {
            ProductCategoryDatabase::add($product->getSku(), $category->getCode());
        }
        return true;
    }

    /**
     * This method select all rows of  product_category from the database by the category_code.
     *
     * @param string $code The code of the category.
     * 
     * @return array A array with all ProductCategoryObject instances from the database where category_code exists.
     */
    public static function selectAllByCategoryCode($code)
    {
        $productCategoryList = [];

        try {
            $conn = static::getConn();
            $sqlQuery = 'SELECT * FROM product_category WHERE category_code = :category_code;';
            $stmt = $conn->prepare($sqlQuery);
            $stmt->bindParam(':category_code', $code, PDO::PARAM_STR);
            $stmt->execute();
            $numArrayProductCategoryList = $stmt->fetchAll(PDO::FETCH_NUM);

            foreach ($numArrayProductCategoryList as $numArrayProductCategory) {
                $productCategoryList[] = new ProductCategoryObject($numArrayProductCategory[0], $numArrayProductCategory[1]);
            }
        } catch (PDOException $e) {
            // The log message
            $logMessage = "A PDOException occurred while trying to select some rows were category_code was equal to `$code` " .
            "from `product_category` database table in `App\Model\Database\ProductCategoryDatabase::selectAllByCategoryCode()`.";
            // Data array for a better context of what happened
            $dataArray = ['PDOExceptionMessage' => $e->getMessage()];
            AppLogger::addDatabaseWarningLog($logMessage, $dataArray);
        }
        return $productCategoryList;
    }

    /**
     * This method select all rows of  product_category from the database by the product_sku.
     *
     * @param string $sku The sku of the product.
     * 
     * @return array A array with all ProductCategoryObject instances from the database where product_sku exists.
     */
    public static function selectAllByProductSku($sku)
    {
        $productCategoryList = [];

        try {
            $conn = static::getConn();
            $sqlQuery = 'SELECT * FROM product_category WHERE product_sku = :product_sku;';
            $stmt = $conn->prepare($sqlQuery);
            $stmt->bindParam(':product_sku', $sku, PDO::PARAM_STR);
            $stmt->execute();
            $numArrayProductCategoryList = $stmt->fetchAll(PDO::FETCH_NUM);

            foreach ($numArrayProductCategoryList as $numArrayProductCategory) {
                $productCategoryList[] = new ProductCategoryObject($numArrayProductCategory[0], $numArrayProductCategory[1]);
            }
        } catch (PDOException $e) {
            // The log message
            $logMessage = "A PDOException occurred while trying to select some rows were product_sku was equal to `$sku` " .
            "from 'product_category' database table in `App\Model\Database\ProductCategoryDatabase::selectAllByProductSku()`.";
            // Data array for a better context of what happened
            $dataArray = ['PDOExceptionMessage' => $e->getMessage()];
            AppLogger::addDatabaseWarningLog($logMessage, $dataArray);
        }
        return $productCategoryList;
    }

    /**
     * This method delete a row of product_category from the database by the prodcut sku and category code.
     *
     * @param string $productSku The sku of the product.
     * @param string $categoryCode The code of the category.
     * @return boolean
     */
    public static function deleteBySkuAndCode($productSku, $categoryCode)
    {
        try {
            $conn = static::getConn();
            $sqlQuery = 'DELETE FROM product_category WHERE product_sku = :product_sku AND category_code=:category_code;';
            $stmt = $conn->prepare($sqlQuery);
            $stmt->bindParam(':product_sku', $productSku, PDO::PARAM_STR);
            $stmt->bindParam(':category_code', $categoryCode, PDO::PARAM_STR);
            $stmt->execute();

            // The log message
            $logMessage = "A row from `product_category` where product_sku `$productSku` and category_code `$categoryCode` was " .
            "successfully deleted from the database table in " .
            "`App\Model\Database\ProductCategoryDatabase::deleteBySkuAndCode()`.";
            // Data array for a better context of what happened
            AppLogger::addDatabaseInfoLog($logMessage);

            return true;
        } catch (PDOException $e) {
            // The log message
            $logMessage = "A PDOException occurred while trying to delete a row from 'product_category' database table " .
            "where sku `$productSku` and category_code `$categoryCode` in " .
            "`App\Model\Database\ProductCategoryDatabase::deleteBySkuAndCode()`.";
            // Data array for a better context of what happened
            $dataArray = ['PDOExceptionMessage' => $e->getMessage()];
            AppLogger::addDatabaseWarningLog($logMessage, $dataArray);
        }
        return false;
    }

    /**
     * This method delete all product_category relationship from the database by the prodcut sku.
     *
     * @param string $productSku The sku of the product.
     * @return boolean
     */
    public static function deleteAllBySku($productSku)
    {
        try {
            $conn = static::getConn();
            $sqlQuery = 'DELETE FROM product_category WHERE product_sku = :product_sku;';
            $stmt = $conn->prepare($sqlQuery);
            $stmt->bindParam(':product_sku', $productSku, PDO::PARAM_STR);
            $stmt->execute();

            // The log message
            $logMessage = "All rows from `product_category` where sku was equal to `" . $productSku . "` were " .
            "successfully deleted from the database table in " .
            "`App\Model\Database\ProductCategoryDatabase::deleteAllBySku()`.";
            // Data array for a better context of what happened
            $dataArray = ['productSku' => $productSku];
            AppLogger::addDatabaseInfoLog($logMessage, $dataArray);

            return true;
        } catch (PDOException $e) {
            // The log message
            $logMessage = "A PDOException occurred while trying to delete rows where sku was equal to `" . $productSku . "` " .
            "from 'product_category' database table in `App\Model\Database\ProductCategoryDatabase::deleteAllBySku()`.";
            // Data array for a better context of what happened
            $dataArray = ['PDOExceptionMessage' => $e->getMessage()];
            AppLogger::addDatabaseWarningLog($logMessage, $dataArray);
        }
        return false;
    }
}

?>