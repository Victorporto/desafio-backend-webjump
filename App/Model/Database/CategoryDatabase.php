<?php

namespace App\Model\Database;

use PDO;
use PDOException;
use App\AppLogger;
use App\Model\Object\CategoryObject;

/**
 * CategoryDatabase class.
 * 
 * It can access the database to Create, Read, Update and Delete data of categories in the database.
 *
 * PHP version 8.0.2
 */
class CategoryDatabase extends \Core\Database
{
    /**
     * This method insert the category object into the database.
     *
     * @param CategoryObject $category The category object.
     * @return boolean True if object was inserted into database, false otherwise.
     */
    public static function insert($category)
    {
        try {
            $code = $category->getCode();
            $name = $category->getName();

            $sql = "INSERT INTO category (code, name)
                    VALUES (:code, :name);";
            $conn = static::getConn();
            $stmt = $conn->prepare($sql);
            $stmt->bindParam(':code', $code, PDO::PARAM_STR);
            $stmt->bindParam(':name', $name, PDO::PARAM_STR);
            $stmt->execute();
        
            // The log message
            $logMessage = "A row was successfully inserted into 'category' database table in " .
            "`App\Model\Database\CategoryDatabase::insert()`.";
            // Data array for a better context of what happened
            $dataArray = ['CategoryObject' => $category->toArray()];
            AppLogger::addDatabaseInfoLog($logMessage, $dataArray);

            return true;
        } catch (PDOException $e) {
            // The log message
            $logMessage = "A PDOException occurred while trying to insert a row into 'category' database table " .
            "in `App\Model\Database\CategoryDatabase::insert()`.";
            // Data array for a better context of what happened
            $dataArray = ['PDOExceptionMessage' => $e->getMessage()];
            AppLogger::addDatabaseWarningLog($logMessage, $dataArray);
        }
        return false;
    }

    /**
     * This method select a category from the database by it's code.
     *
     * @param string $code The code of the category.
     * 
     * @return mix CategoryObject instance from the database if code exists, false otherwise.
     */
    public static function selectByCode($code)
    {
        try {
            $conn = static::getConn();
            $sqlQuery = 'SELECT * FROM category WHERE code = :code;';
            $stmt = $conn->prepare($sqlQuery);
            $stmt->bindParam(':code', $code, PDO::PARAM_STR);
            $stmt->execute();
            $numArrayCategory = $stmt->fetch(PDO::FETCH_NUM);

            if ($numArrayCategory === false) {
                return false;
            }

            $code = $numArrayCategory[0];
            $name = $numArrayCategory[1];

            return new CategoryObject($code, $name);
        } catch (PDOException $e) {
            // The log message
            $logMessage = "A PDOException occurred while trying to select a row from 'category' database table " .
            "in `App\Model\Database\CategoryDatabase::selectByCode()`.";
            // Data array for a better context of what happened
            $dataArray = ['PDOExceptionMessage' => $e->getMessage()];
            AppLogger::addDatabaseWarningLog($logMessage, $dataArray);
        }
        return false;
    }

    /**
     * This method edit a category object from the database by it's code.
     *
     * @param CategoryObject $category The category object with edited data.
     * @return boolean True if object was edited in database, false otherwise.
     */
    public static function update($category) {
        try {
            $code = $category->getCode();
            $name = $category->getName();

            $sql = "UPDATE category SET name=:name WHERE code=:code;";
            $conn = static::getConn();
            $stmt = $conn->prepare($sql);
            $stmt->bindParam(':code', $code, PDO::PARAM_STR);
            $stmt->bindParam(':name', $name, PDO::PARAM_STR);
            $stmt->execute();

            $logMessage = "A row was were code was `" . $code . "` successfully edited in `category` " .
            "database table in `App\Model\Database\CategoryDatabase::update()`.";
            $dataArray = ['editedCategoryObject' => $category->toArray()];
            AppLogger::addDatabaseInfoLog($logMessage, $dataArray);

            return true;
        } catch (PDOException $e) {
            $logMessage = "A PDOException occurred while trying to edit a row from `category` database table " .
            "in `App\Model\Database\CategoryDatabase::update()`.";
            $dataArray = [
                'editedCategoryData' => $category->toArray(),
                'PDOExceptionMessage' => $e->getMessage()
            ];
            AppLogger::addDatabaseWarningLog($logMessage, $dataArray);
        }
        return false;
    }

    /**
     * Select all categories from the database.
     *
     * @return array $categoryList The list of all CategoryObject on the database.
     */
    public static function selectAll()
    {
        $categoryList = [];

        try {
            $conn = static::getConn();
            $sqlQuery = 'SELECT * FROM category ORDER BY name ASC;';
            $stmt = $conn->prepare($sqlQuery);
            $stmt->execute();
            $numArrayCategoryList = $stmt->fetchAll(PDO::FETCH_NUM);

            foreach ($numArrayCategoryList as $numArrayCategory) {
                $categoryList[] = new CategoryObject($numArrayCategory[0], $numArrayCategory[1]);
            }
        } catch (PDOException $e) {
            // The log message
            $logMessage = "A PDOException occurred while trying to select all rows from 'category' database table " .
            "in `App\Model\Database\CategoryDatabase::selectAll()`.";
            // Data array for a better context of what happened
            $dataArray = ['PDOExceptionMessage' => $e->getMessage()];
            AppLogger::addDatabaseWarningLog($logMessage, $dataArray);
        }

        return $categoryList;
    }

    /**
     * This method delete a category from the database by it's code.
     *
     * @param string $code The code of the category.
     * @return void
     */
    public static function deleteByCode($code)
    {
        try {
            $conn = static::getConn();
            $sqlQuery = 'DELETE FROM category WHERE code = :code;';
            $stmt = $conn->prepare($sqlQuery);
            $stmt->bindParam(':code', $code, PDO::PARAM_STR);
            $stmt->execute();

            // The log message
            $logMessage = "A row from `category` where code was equal to `" . $code . "` was successfully deleted from " .
            "the database table in `App\Model\Database\CategoryDatabase::deleteByCode()`.";
            // Data array for a better context of what happened
            $dataArray = ['code' => $code];
            AppLogger::addDatabaseInfoLog($logMessage, $dataArray);

            return true;
        } catch (PDOException $e) {
            // The log message
            $logMessage = "A PDOException occurred while trying to delete a row where code was equal to `" . $code . "` " .
            "from `category` database table in `App\Model\Database\CategoryDatabase::deleteByCode()`.";
            // Data array for a better context of what happened
            $dataArray = ['PDOExceptionMessage' => $e->getMessage()];
            AppLogger::addDatabaseWarningLog($logMessage, $dataArray);
        }
        return false;
    }
}

?>