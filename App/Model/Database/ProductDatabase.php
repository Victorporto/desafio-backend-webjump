<?php

namespace App\Model\Database;

use PDO;
use PDOException;
use App\AppLogger;
use App\Model\Object\ProductObject;
use App\Model\Object\CategoryObject;
use App\Model\Database\CategoryDatabase;
use App\Model\Database\ProductCategoryDatabase;

/**
 * ProductDatabase class.
 * 
 * It can access the database to Create, Read, Update and Delete data of products in the database.
 *
 * PHP version 8.0.2
 */
class ProductDatabase extends \Core\Database
{
    /**
     * This method insert the product object into the database.
     *
     * @param ProductObject $product The product object.
     * @return boolean True if object was inserted into database. False otherwise.
     */
    public static function insert($product)
    {
        try {
            $sku = $product->getSku();
            $name = $product->getName();
            $price = $product->getPrice();
            $description = $product->getDescription();
            $quantity = $product->getQuantity();

            $sql = "INSERT INTO product (sku, name, price, description, quantity)
                    VALUES (:sku, :name, :price, :description, :quantity);";
            $conn = static::getConn();
            $stmt = $conn->prepare($sql);
            $stmt->bindParam(':sku', $sku, PDO::PARAM_STR);
            $stmt->bindParam(':name', $name, PDO::PARAM_STR);
            $stmt->bindParam(':price', $price, PDO::PARAM_STR);
            $stmt->bindParam(':description', $description, PDO::PARAM_STR);
            $stmt->bindParam(':quantity', $quantity, PDO::PARAM_INT);
            $stmt->execute();

            // The log message
            $logMessage = "A row was successfully inserted into `product` database table in " .
            "`App\Model\Database\ProductDatabase::insert()`.";
            // Data array for a better context of what happened
            $dataArray = ['ProductObject' => $product->toArray()];
            AppLogger::addDatabaseInfoLog($logMessage, $dataArray);

            ProductCategoryDatabase::insertProductCategoryListOfProduct($product);
            return true;
        } catch (PDOException $e) {
            // The log message
            $logMessage = "A PDOException occurred while trying to insert a row into `product` database table " .
            "in `App\Model\Database\ProductDatabase::insert()`.";
            // Data array for a better context of what happened
            $dataArray = ['PDOExceptionMessage' => $e->getMessage()];
            AppLogger::addDatabaseWarningLog($logMessage, $dataArray);
        }
        return false;
    }

    /**
     * This method select a product from the database by it's sku.
     *
     * @param string $sku The sku of the product.
     * 
     * @return mix A ProductObject instance from the database if $sku exists, false otherwise.
     */
    public static function selectBySku($sku)
    {
        try {
            $conn = static::getConn();
            $sqlQuery = 'SELECT * FROM product WHERE sku = :sku;';
            $stmt = $conn->prepare($sqlQuery);
            $stmt->bindParam(':sku', $sku, PDO::PARAM_STR);
            $stmt->execute();
            $numArrayProduct = $stmt->fetch(PDO::FETCH_NUM);

            if ($numArrayProduct === false) {
                return false;
            }

            $sku = $numArrayProduct[0];
            $name = $numArrayProduct[1];
            $price = $numArrayProduct[2];
            $description = $numArrayProduct[3];
            $quantity = $numArrayProduct[4];
            $categoryList = ProductDatabase::selectProductCategoryListBySku($sku);

            return new ProductObject($sku, $name, $price, $description, $quantity, $categoryList);
        } catch (PDOException $e) {
            // The log message
            $logMessage = "A PDOException occurred while trying to select a row from `product` database table " .
            "in `App\Model\Database\ProductDatabase::selectBySku()`.";
            // Data array for a better context of what happened
            $dataArray = ['PDOExceptionMessage' => $e->getMessage()];
            AppLogger::addDatabaseWarningLog($logMessage, $dataArray);
        }
        return false;
    }

    /**
     * Select all categories object from the database, that has relationship with the $sku product.
     *
     * @param string $sku The sku of the product.
     * @return CategoryObject[] $categoryList The list of all categories objects.
     */
    private static function selectProductCategoryListBySku($sku)
    {
        $categoryList = [];
        try {
            $conn = static::getConn();
            $sqlQuery = 'SELECT * FROM product_category WHERE product_sku=:product_sku;';
            $stmt = $conn->prepare($sqlQuery);
            $stmt->bindParam(':product_sku', $sku, PDO::PARAM_STR);
            $stmt->execute();
            $numArrayProductCategoryList = $stmt->fetchAll(PDO::FETCH_NUM);

            foreach ($numArrayProductCategoryList as $numArrayProductCategory) {
                $categoryCode = $numArrayProductCategory[1];
                $categoryList[] = CategoryDatabase::selectByCode($categoryCode);
            }
        } catch (PDOException $e) {
            // The log message
            $logMessage = "A PDOException occurred while trying to select rows where product_sku was equal to `" . $sku . 
            "` from `product_category` database table in " .
            "`App\Model\Database\ProductDatabase::selectProductCategoryListBySku()`.";
            // Data array for a better context of what happened
            $dataArray = ['PDOExceptionMessage' => $e->getMessage()];
            AppLogger::addDatabaseWarningLog($logMessage, $dataArray);
        }
        return $categoryList;
    }

    /**
     * Select all products from the database.
     *
     * @return ProductObject[] $productList The list of all products.
     */
    public static function selectAll()
    {
        $productList = [];
        try {
            $conn = static::getConn();
            $sqlQuery = 'SELECT * FROM product ORDER BY name ASC;';
            $stmt = $conn->prepare($sqlQuery);
            $stmt->execute();
            $numArrayProductList = $stmt->fetchAll(PDO::FETCH_NUM);

            foreach ($numArrayProductList as $numArrayProduct) {
                $sku = $numArrayProduct[0];
                $name = $numArrayProduct[1];
                $price = $numArrayProduct[2];
                $description = $numArrayProduct[3];
                $quantity = $numArrayProduct[4];

                $categoryList = ProductDatabase::selectProductCategoryListBySku($sku);
                $productList[] = new ProductObject($sku, $name, $price, $description, $quantity, $categoryList);
            }
        } catch (PDOException $e) {
            // The log message
            $logMessage = "A PDOException occurred while trying to select all rows from `product` database table " .
            "in `App\Model\Database\ProductDatabase::selectAll()`.";
            // Data array for a better context of what happened
            $dataArray = ['PDOExceptionMessage' => $e->getMessage()];
            AppLogger::addDatabaseWarningLog($logMessage, $dataArray);
        }
        return $productList;
    }

    /**
     * This method edit a product object from the database by it's sku.
     *
     * @param ProductObject $product The product object with edited data.
     * @return boolean 
     */
    public static function update($product) {
        try {
            $sku = $product->getSku();
            $name = $product->getName();
            $price = $product->getPrice();
            $description = $product->getDescription();
            $quantity = $product->getQuantity();

            $sql = "UPDATE product 
                SET name=:name, price=:price, description=:description, quantity=:quantity 
                WHERE sku=:sku;";
            $conn = static::getConn();
            $stmt = $conn->prepare($sql);
            $stmt->bindParam(':name', $name, PDO::PARAM_STR);
            $stmt->bindParam(':price', $price, PDO::PARAM_STR);
            $stmt->bindParam(':description', $description, PDO::PARAM_STR);
            $stmt->bindParam(':quantity', $quantity, PDO::PARAM_INT);
            $stmt->bindParam(':sku', $sku, PDO::PARAM_STR);
            $stmt->execute();

            $logMessage = "A row was were sku was `" . $sku . "` successfully edited in `product` " .
            "database table in `App\Model\Database\ProductDatabase::update()`.";
            $dataArray = ['editedProductObject' => $product->toArray()];
            AppLogger::addDatabaseInfoLog($logMessage, $dataArray);

            return true;
        } catch (PDOException $e) {
            $logMessage = "A PDOException occurred while trying to edit a row from `product` database table " .
            "in `App\Model\Database\ProductDatabase::update()`.";
            $dataArray = [
                'editedProductObject' => $product->toArray(),
                'PDOExceptionMessage' => $e->getMessage()
            ];
            AppLogger::addDatabaseWarningLog($logMessage, $dataArray);
        }
        return false;
    }
    
    /**
     * This method delete a product from the database by it's sku.
     *
     * @param string $sku The sku of the product.
     * @return boolean
     */
    public static function deleteBySku($sku)
    {
        ProductCategoryDatabase::deleteAllBySku($sku);

        try {
            $conn = static::getConn();
            $sqlQuery = 'DELETE FROM product WHERE sku = :sku;';
            $stmt = $conn->prepare($sqlQuery);
            $stmt->bindParam(':sku', $sku, PDO::PARAM_STR);
            $stmt->execute();

            // The log message
            $logMessage = "A row from `product` where sku was equal to `" . $sku . "` was successfully deleted from " .
            "the database table in `App\Model\Database\ProductDatabase::deleteBySku()`.";
            // Data array for a better context of what happened
            $dataArray = ['sku' => $sku];
            AppLogger::addDatabaseInfoLog($logMessage, $dataArray);

            return true;
        } catch (PDOException $e) {
            // The log message
            $logMessage = "A PDOException occurred while trying to delete a row where sku was equal to `" . $sku . "` " .
            "from `product` database table in `App\Model\Database\ProductDatabase::deleteBySku()`.";
            // Data array for a better context of what happened
            $dataArray = ['PDOExceptionMessage' => $e->getMessage()];
            AppLogger::addDatabaseWarningLog($logMessage, $dataArray);
        }
        return false;
    }
}

?>