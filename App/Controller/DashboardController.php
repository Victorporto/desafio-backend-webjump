<?php

namespace App\Controller;

use Core\View;
use App\Model\Database\ProductDatabase;

/**
 * Dashboard controller
 *
 * PHP version 8.0.2
 */
class DashboardController
{

    /**
     * This action render the dashboard page.
     *
     * @return void
     */
    public function dashboardPage()
    {
        $data['productList'] = ProductDatabase::selectAll();
        View::render('Dashboard/dashboard.php', $data);
    }

}

?>