<?php

namespace App\Controller;

use Core\View;
use App\Model\Util\SanitizeUtil;
use App\Model\Service\CategoryService;
use App\Model\Database\CategoryDatabase;
use App\Model\Database\ProductCategoryDatabase;
use App\Model\Database\ProductDatabase;

/**
 * Category controller
 *
 * PHP version 8.0.2
 */
class CategoryController extends \Core\Controller
{
    /**
     * This action render the show page.
     *
     * @return void
     */
    public function showPage()
    {
        $data['messages'] = $this->getMessageList();
        $data['errors'] = $this->getErrorList();

        $categoryList = CategoryDatabase::selectAll();
        $data["categoryList"] = $categoryList;
        View::render("Category/categories.php", $data);
    }

    /**
     * This action render the add page.
     *
     * @return void
     */
    public function addPage()
    {
        $data['messages'] = $this->getMessageList();
        $data['errors'] = $this->getErrorList();

        View::render("Category/add-category.php", $data);
    }

    /**
     * This action add a category.
     *
     * @return void
     */
    public function add()
    {
        $data = SanitizeUtil::recursiveHtmlSpecialChars($_POST);
        $categoryService = new CategoryService();

        if ($categoryService->add($data['code'], $data['name'])) { // Insertion done
            $this->addMessage("Registration success!");
        } else { // Something went wrong while trying to insert the category
            $this->addError($categoryService->getErrorList());
        }

        $this->redirect("/category-controller/add-page");
    }

    /**
     * This action render the edit page.
     *
     * @return void
     */
    public function editPage()
    {
        $data['messages'] = $this->getMessageList();
        $data['errors'] = $this->getErrorList();

        if (isset($_POST['code'])) { // It's the first time accessing this action
            $code =  SanitizeUtil::recursiveHtmlSpecialChars($_POST['code']);
            $_SESSION['edit_category_code'] = $code;
        } else { // Reload the category code to edit
            $code =  $_SESSION['edit_category_code'];
        } 

        $data['category'] = CategoryDatabase::selectByCode($code);

        // Get all products that belong to the category, to show the user
        $data['productList'] = array_map(function($productCategory) {
            return ProductDatabase::selectBySku($productCategory->getProductSku());
        }, ProductCategoryDatabase::selectAllByCategoryCode($code));

        if ( ! empty($data['category'])) { // Render the view page if category was found
            View::render("Category/edit-category.php", $data);
        } else { // Redirect to show page if none was found
            $this->redirect("/category-controller/show-page");
        }
    }

    /**
     * This action edit a category.
     *
     * @return void
     */
    public function edit()
    {
        $data = SanitizeUtil::recursiveHtmlSpecialChars($_POST);
        $categoryService = new CategoryService();

        if ($categoryService->edit($data['code'], $data['name'])) { // Edition done
            unset($_SESSION['edit_category_code']); // Unset the code it's not necessary anymore
            $this->addMessage("Edition success!");
            $this->redirect("/category-controller/show-page");
        } else { // Something went wrong while trying to edit the category
            $this->addError($categoryService->getErrorList());
            $this->redirect("/category-controller/edit-page");
        }
    }

    /**
     * This action delete a category.
     *
     * @return void
     */
    public function delete()
    {
        $data = SanitizeUtil::recursiveHtmlSpecialChars($_POST);
        $categoryService = new CategoryService();

        if ($categoryService->delete($data['code'])) { // Insertion done
            $this->addMessage("Category deleted!");
        } else { // Something went wrong while trying to delete the category
            $this->addError($categoryService->getErrorList());
        }

        $this->redirect("/category-controller/show-page");
    }
}

?>