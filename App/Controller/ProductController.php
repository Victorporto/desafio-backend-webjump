<?php

namespace App\Controller;

use Core\View;
use App\Model\Util\SanitizeUtil;

use App\Model\Service\ProductService;
use App\Model\Database\ProductDatabase;
use App\Model\Database\CategoryDatabase;

/**
 * Product controller
 *
 * PHP version 8.0.2
 */
class ProductController extends \Core\Controller
{

    /**
     * This action render the show page.
     *
     * @return void
     */
    public function showPage()
    {
        $data['messages'] = $this->getMessageList();
        $data['errors'] = $this->getErrorList();
        
        $productList = ProductDatabase::selectAll();
        $data["productList"] = $productList;
        View::render("Product/products.php", $data);
    }

    /**
     * This action render the add page.
     *
     * @return void
     */
    public function addPage()
    {
        $data['messages'] = $this->getMessageList();
        $data['errors'] = $this->getErrorList();

        $data['categoryList'] = CategoryDatabase::selectAll();
        View::render("Product/add-product.php", $data);
    }

    /**
     * This action add a product.
     *
     * @return void
     */
    public function add()
    {
        $data = SanitizeUtil::recursiveHtmlSpecialChars($_POST);
        $productService = new ProductService();

        if ($productService->add(
                $data["sku"],
                $data["name"],
                $data["price"],
                $data["description"],
                $data["quantity"],
                $data['category_code_list']
            )
        ) { // Insertion done
            $this->addMessage("Registration success!");
        } else { // Something went wrong while trying to insert the product
            $this->addError($productService->getErrorList());
        }

        $this->redirect("/product-controller/add-page");
    }

    /**
     * This action render the edit page.
     *
     * @return void
     */
    public function editPage()
    {
        $data['messages'] = $this->getMessageList();
        $data['errors'] = $this->getErrorList();

        if (isset($_POST['sku'])) { // It's the first time accessing this action
            $sku =  SanitizeUtil::recursiveHtmlSpecialChars($_POST['sku']);
            $_SESSION['edit_product_sku'] = $sku;
        } else { // Reload the product sku to edit
            $sku =  $_SESSION['edit_product_sku'];
        } 

        $data['product'] = ProductDatabase::selectBySku($sku);
        $data['categoryList'] = CategoryDatabase::selectAll();

        if ( ! empty($data['product'])) { // Render the view page if product was found
            View::render("Product/edit-product.php", $data);
        } else { // Redirect to show page if none was found
            $this->redirect("/product-controller/show-page");
        }
    }

    /**
     * This action edit a category.
     *
     * @return void
     */
    public function edit()
    {
        $data = SanitizeUtil::recursiveHtmlSpecialChars($_POST);
        $productService = new ProductService();

        if ($productService->edit(
                $data["sku"],
                $data["name"],
                $data["price"],
                $data["description"],
                $data["quantity"],
                $data['category_code_list']
            )
        ) { // Edition done
            unset($_SESSION['edit_product_sku']); // Unset the sku it's not necessary anymore
            $this->addMessage("Edition success!");
            $this->redirect("/product-controller/show-page");
        } else { // Something went wrong while trying to edit the product
            $this->addError($productService->getErrorList());
            $this->redirect("/product-controller/edit-page");
        }
    }

    /**
     * This action delete a product.
     *
     * @return void
     */
    public function delete()
    {
        $data = SanitizeUtil::recursiveHtmlSpecialChars($_POST);
        $productService = new ProductService();

        if ($productService->delete($data["sku"])) { // Deletion done
            $this->addMessage("Product deleted!");
        } else { // Something went wrong while trying to delete the product
            $this->addError($productService->getErrorList());
        }

        $this->redirect("/product-controller/show-page");
    }
}

?>