# Desafio WEBJUMP - Victor Porto Lambert
Desafio concluído por **Victor Porto Lambert** -  - [perfil no github](https://github.com/victorporto), como parte de entrevista da empresa.

# Como o Projeto foi desenvolvido
O projeto foi implementado seguindo o padrão de projeto MVC e seguindo princípios SOLID.

### Estrutura de Pastas da Aplicação
```
- webapp
  - App (todo o código que descrever o comportamento da nossa aplicação, seguindo o padrão MVC)
    - Controller (pasta com os controllers)
    - LogFiles (pasta com logs)
    - Model
      - Database (classes que acessam o banco de dados)
      - Object (classes para criação e manipulação dos objetos)
      - Service (contém regras de negócio e validações)
      - Util (contém uma classe de sanitização dos dados de entrada)
    - View
  - Core (classes core que determinam o comportamento dos Controllers, algumas classes da Model, roteamento e renderização das views)
  - Public (raiz do servidor)
  - vendor (bibliotecas externas)
  - .gitignore
  - composer.json
  - composer.lock
  - readme.md
  - webstore.sql (arquivo com estrutura do banco de dados)
```

### Bibliotecas Externas
Foi utilizado o [monolog](https://github.com/Seldaek/monolog) para implementar funcionalidade de logs na aplicação

# Configurando no Windows

## Programas Necessários 
No windows é fortemente recomendado baixar e instalar o
[XAMPP](https://www.apachefriends.org/pt_br/index.html) e o [Composer](https://getcomposer.org/), para facilitar
a configuração, visto que o XAMPP reune todos os programas necessários (com excessão do Composer, que
tem de ser baixado separadamente). A configuração será descrita utilizando o XAMPP e Composer.
Mas se preferir você pode baixar e instalar cada programa individualmente. Os programas utilizados estão listados abaixo.

* PHP 8.0.2+
* Apache
* MySql
* Composer
* MyPhpAdmin (Opcional, mas recomendado)

## Clonando o Repositório
Ao terminar a instalação do XAMPP, clone o repositório para o diretório webapp `C:\xampp\htdocs\webapp` Você pode dar
o nome que quiser para a pasta `webapp` mas lembre-se de refletir essa alteração nos outros passos da configuração.

## Configurando o APACHE (XAMPP)
Após clonar o repositório na pasta do passo anterior, precisaremos alterar a pasta raiz do servidor apache.
Abra o arquivo localizado em `C:\xampp\apache\conf\httpd.conf` e onde houver:

```
DocumentRoot "C:/xampp/htdocs"
<Directory "C:/xampp/htdocs">
  Options Indexes FollowSymLinks Includes ExecCGI
  AllowOverride none
  Require all granted
</Directory>
```

Substituir por:
```
DocumentRoot "C:/xampp/htdocs/webapp/Public"
<Directory "C:/xampp/htdocs/webapp/Public">
  Options Indexes FollowSymLinks Includes ExecCGI
  AllowOverride all
  Require all granted
</Directory>
```

Haverá alguns comentários `#` descrevendo as configurações que estamos alterando, você pode ignorá-las.

## Configurando o Composer
Navegue para o diretório `C:\xampp\htdocs\webapp\App` com o Visual Studio Code ou Windows PowerShell e execute o compando `composer install` para
instalar as dependências do projeto. O composer irá configurar o `autoload` da nossa aplicação e instalar o [monolog](https://github.com/Seldaek/monolog)
para que nossa aplicação possa gerar arquivos de log.

## Configurando o MySQL
A configuração descrita nesta secção está de acordo com o arquivo de configuração da nossa aplicação, localizado em `wepapp/App/Config.php`
Quaisquer mudanças feitas na configuração do banco de dados deve se refletir no arquivo de configuração, visto que a aplicação utiliza ele para
saber o nome do banco de dados, usuário do banco, etc.

1. No MySql, crie um novo banco de dados chamado `webstore` com collate utf8_unicode_ci.
2. Crie um usuário de nome `webstore_user` e senha `webstore_user_password`, com permissões de `INSERT`, `SELECT`, `UPDATE` e `DELETE` no banco criado no passo anterior.
3. Em seguida, selecione o banco `webstore` e execute os comandos presentes dentro do arquivo `webapp/webstore.sql` Ou se você estiver usando o PhpMyAdmin você pode importar o arquivo para o banco `webstore`.

# Tudo pronto! Pode começar a testar as funcionalidades da aplicação.