<?php

namespace Core;

use PDO;
use PDOException;
use App\Config;

/**
 * Base Database
 *
 * PHP version 8.0.2
 */
abstract class Database
{
    /**
     * Get database connection on 'webstore' database using PDO.
     *
     * @return mixed
     */
    protected static function getConn()
    {
        static $conn = null;

        if ($conn === null) {
            try {
                $dsn = 'mysql:host=' . Config::DB_HOST . ';dbname=' . Config::DB_NAME . ';charset=utf8';
                $conn = new PDO($dsn, Config::DB_USER, Config::DB_PASSWORD);
            } catch (PDOException $e) {
                echo 'An error occurred while trying to connect to the database: ' . $e->getMessage();
            }
        }

        return $conn;
    }
}

?>
