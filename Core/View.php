<?php

namespace Core;

/**
 * Base View
 *
 * PHP version 8.0.2
 */
class View
{
    /**
     * Render a view file
     *
     * @param string $view The view file.
     * @param array $data It's not necessary to pass this argument.
     *
     * @return void
     */
    public static function render($view, $args = [])
    {
        extract($args, EXTR_SKIP);

        // Relative to Core directory.
        $file = "../App/View/$view";

        if (is_readable($file)) {
            require $file;
        } else {
            echo "File: '$file' not found.";
        }
    }
}

?>
