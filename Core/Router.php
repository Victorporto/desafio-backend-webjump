<?php

namespace Core;

/**
 * Router class for basic front controller behavior.
 *
 * PHP version 8.0.2
 */
class Router
{
    /**
     * Associative array of routes.
     * 
     * @var array
     */
    protected $routes = [];

    /**
     * Parameters from the matched route.
     *
     * @var array
     */
    protected $params = [];

    /**
     * Add a route.
     *
     * @param string $route The route URL.
     * @param array  $params Parameters (controller, action, etc.).
     *
     * @return void
     */
    public function add($route, $params = [])
    {
        // Convert the route to a regular expression: escape forward slashes
        $route = preg_replace('/\//', '\\/', $route);

        // Convert variables. E.g.: {controller}
        $route = preg_replace('/\{([a-z]+)\}/', '(?P<\1>[a-z-]+)', $route);

        // Convert variables with custom regular expressions. E.g.: {id:\d+}
        $route = preg_replace('/\{([a-z]+):([^\}]+)\}/', '(?P<\1>\2)', $route);

        // Add start and end delimiters, and case sensitive flag
        $route = '/^' . $route . '$/i';

        $this->routes[$route] = $params;
    }

    /**
     * Get all the routes.
     *
     * @return array
     */
    public function getRoutes()
    {
        return $this->routes;
    }

    /**
     * Check if the $url match any route on the $routes array.
     * If a route is found, set the $params property.
     *
     * @param string $url The URL the user tried to access.
     *
     * @return boolean True if a match found, false otherwise.
     */
    public function match($url)
    {
        foreach ($this->routes as $route => $params) {
            if (preg_match($route, $url, $matches)) {
                foreach ($matches as $key => $match) {
                    if (is_string($key)) {
                        $params[$key] = $match;
                    }
                }
                $this->params = $params;
                return true;
            }
        }

        return false;
    }

    /**
     * Get the currently matched parameters
     *
     * @return array
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * Dispatch the route, creating the controller object and running the action method.
     *
     * @param string $url The URL.
     *
     * @return void
     */
    public function dispatch($url)
    {
        $url = $this->removeQueryStringVariables($url);

        if ($this->match($url)) {
            $controller = $this->params['controller'];
            $controller = $this->convertToStudlyCaps($controller);
            $controller = $this->getNamespace() . $controller;

            if (class_exists($controller)) {
                $controllerObject = new $controller($this->params);
                $action = $this->params['action'];
                $action = $this->convertToCamelCase($action);

                if (preg_match('/action$/i', $action) == 0) {
                    $controllerObject->$action();
                } else {
                    throw new \ExpostalCodetion("Method $action in controller $controller cannot be called directly - 
                    remove the Action statefix to call this method");
                }
            } else {
                echo "Controller class $controller not found.";
            }
        } else {
            echo 'No route matched.';
        }
    }

    /**
     * Convert the strin with hyphens to StudlyCaps. E.g.: post-authors => PostAuthors
     *
     * @param string $string The string to convert.
     *
     * @return string
     */
    protected function convertToStudlyCaps($string)
    {
        return str_replace(' ', '', ucwords(str_replace('-', ' ', $string)));
    }

    /**
     * Convert the string with hyphens to camelCase. E.g.: add-new => addNew
     *
     * @param string $string The string to convert.
     *
     * @return string
     */
    protected function convertToCamelCase($string)
    {
        return lcfirst($this->convertToStudlyCaps($string));
    }

    /**
     * Remove from the URL any variables on $_SERVER['QUERY_STRING'].
     * We need to do this before trying to match the url with a route, as the full $_SERVER['QUERY_STRING'] can have
     * variables on it.
     * 
     * For example:
     *
     *   URL                           $_SERVER['QUERY_STRING']  Route
     *   -------------------------------------------------------------------
     *   localhost                     ''                        ''
     *   localhost/?                   ''                        ''
     *   localhost/?page=1             page=1                    ''
     *   localhost/posts?page=1        posts&page=1              posts
     *   localhost/posts/index         posts/index               posts/index
     *   localhost/posts/index?page=1  posts/index&page=1        posts/index
     *
     * A URL of the format localhost/?page (one variable name, no value) won't
     * work however. (The .htaccess file converts the first '?' to '&' when
     * it's passed through to the $_SERVER variable).
     *
     * @param string $url The full URL
     *
     * @return string The URL with the query string variables removed
     */
    protected function removeQueryStringVariables($url)
    {
        if ($url != '') {
            $parts = explode('&', $url, 2);

            if (strpos($parts[0], '=') === false) {
                $url = $parts[0];
            } else {
                $url = '';
            }
        }

        return $url;
    }

    /**
     * Get the namespace for the controller class. The namespace defined in the route parameters
     * is added, if present.
     *
     * @return string The requested URL.
     */
    protected function getNamespace()
    {
        // Default namespace for controllers.
        $namespace = 'App\Controller\\';

        if (array_key_exists('namespace', $this->params)) {
            // Specify the another folder/namespace to look for the controller.
            $namespace .= $this->params['namespace'] . '\\';
        }

        return $namespace;
    }
}

?>
