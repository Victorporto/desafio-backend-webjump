<?php

namespace Core;

/**
 * Base Controller
 *
 * PHP version 8.0.2
 */
abstract class Controller
{
    /**
     * Parameters from matched route
     *
     * @var array
     */
    protected $route_params = [];

    /**
     * Class constructor
     *
     * @param array $route_params Parameters from the route.
     *
     * @return void
     */
    public function __construct($route_params)
    {
        $this->route_params = $route_params;
    }

    /**
     * Redirect to a different action
     *
     * @param string $url The URL.
     *
     * @return void
     */
    public function redirect($url)
    {
        header('Location: http://' . $_SERVER['HTTP_HOST'] . $url, true, 303);
        exit();
    }

    /**
     * Add a message or a list of messages into $_SESSION['messages'].
     *
     * @param mix $message The message to be added to the list of messages. Accepts a string or array of messages.
     * @return void
     */
    public function addMessage($message)
    {
        if (gettype($message) === 'string') {
            $_SESSION['messages'][] = $message;
        } else if (gettype($message) === 'array') {

            foreach ($message as $msg) {
                $_SESSION['messages'][] = $msg;
            }
        }
    }

    /**
     * Add a error or a list of errors into $_SESSION['errors'].
     *
     * @param mix $error The error to be added to the list of errors. Accepts a string or array of errors.
     * @return void
     */
    public function addError($error)
    {
        if (gettype($error) === 'string') {
            $_SESSION['errors'][] = $error;
        } else if (gettype($error) === 'array') {

            foreach ($error as $err) {
                $_SESSION['errors'][] = $err;
            }
        }
    }

    /**
     * Get the messages of $_SESSION.
     *
     * @return mix An array of messages to be displayed on the view. Null $_SESSION has no message.
     */
    public function getMessageList()
    {
        isset($_SESSION['messages']) ? $messages = $_SESSION['messages'] : $messages = null;
        unset($_SESSION['messages']);

        return $messages;
    }

    /**
     * Get the errors of $_SESSION.
     *
     * @return mix An array of errors to be displayed on the view. Null $_SESSION has no message.
     */
    public function getErrorList()
    {
        isset($_SESSION['errors']) ? $errors = $_SESSION['errors'] : $errors = null;
        unset($_SESSION['errors']);
        
        return $errors;
    }
}

?>
