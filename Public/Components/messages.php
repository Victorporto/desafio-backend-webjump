<?php

// Show on the view any messages that need to be so
if (isset($messages) && !empty($messages)) {
    $showMessage = "<ul>";

    foreach ($messages as $message) {
        $showMessage .= '<li>' . $message . '</li>';
    }

    $showMessage .= '</ul>';
    echo $showMessage;
}

// Show on the view any errors that need to be so
if (isset($errors) && !empty($errors)) {
    $showMessage =
        "<div><p>Verify the information bellow:</p><ul>";

    foreach ($errors as $error) {
        $showMessage .= '<li>' . $error . '</li>';
    }

    $showMessage .= '</ul></div>';
    echo $showMessage;
}

?>