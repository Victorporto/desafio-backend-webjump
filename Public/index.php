<?php

/**
 * Front Controller
 * 
 * PHP version 8.0.2
 */

// Composer autoload
require_once '../Vendor/autoload.php';

// Start the session
session_start();

//Routing
$router = new Core\Router();

// Add new routes
$router->add('', ['controller' => 'DashboardController', 'action' => 'dashboard-page']);
$router->add('{controller}/{action}');

$router->dispatch($_SERVER['QUERY_STRING']);
?>